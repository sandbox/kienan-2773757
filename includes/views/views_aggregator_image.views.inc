<?php

/**
 * @file
 * Views include file for the views_aggregator_image module.
 */

function views_aggregator_image_views_data_alter(&$data) {
   $data['aggregator_feed']['image'] = array(
      'title' => t('Feed Image URL'),
      'help' => t('The raw channel image url set for the feed.'),
      'field' => array(
         'handler' => 'views_handler_field_url',
         'click sortable' => FALSE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
   );
}
